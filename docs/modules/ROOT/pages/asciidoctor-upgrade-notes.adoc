= Asciidoctor Upgrade Notes

Antora 3 uses Asciidoctor.js 2.2.x (Asciidoctor 2.0.x) instead of Asciidoctor.js 1.5.9 (Asciidoctor 1.5.8) to process content files.
Asciidoctor 2 introduces many new features and a few substantive changes to existing features.

== Asciidoctor 2 feature changes

The following table describes the new behavior of existing Asciidoctor features and suggests the actions you should take prior to upgrading to Antora 3.

|===
|Feature |New behavior |Action

|Non-AsciiDoc files inserted into a verbatim block (listing, literal, and source blocks) with an include directive
|Trailing space characters aren't removed, tabs aren't expanded, and newlines aren't normalized in non-AsciiDoc files that are inserted into a verbatim block with an include directive.
This can change how the output is displayed.
.3+.^a|* Update non-AsciiDoc files that use a mix of tabs and spaces or inconsistent newlines if their content isn't displaying as expected when published.
* Replace any tabs in non-AsciiDoc files if the `indent` attribute is set but not working as expected.
* Remove trailing space characters from non-AsciiDoc files, especially if using callouts in the file content or applying the `indent` attribute.

|Non-AsciiDoc files with the `indent` attribute set on the verbatim block they're inserted into with an include directive
|Since tabs aren't expanded, the `indent` attribute may not work as expected.

|Non-AsciiDoc files with callouts that are inserted into a verbatim block with an include directive
|Callout numbers may no longer be detected if followed by a space character.

|Delimited listing blocks without an explicit style when `source-language` is set
|Delimited listing blocks (`+----+`) that don't have an explicit style are automatically promoted to source blocks if `source-language` is set in the document, component descriptor, or playbook.
This may result in unwanted syntax highlighting.
a|If `source-language` isn't set, you don't need to take any action.

If `source-language` is set, do the following:

* Assign the style `listing` to any unstyled delimited listing blocks that shouldn't be promoted to source blocks.
You can also change them to delimited literal blocks (`+....+`).
* (optional) Remove the style `source` from delimited listing blocks that should be promoted to source blocks.
The `source` style is applied automatically.

|Source blocks that aren't assigned a language
|The language `none` is automatically assigned to source blocks (`source`) when no language is set on the block or by `source-language`.

The block is styled like other source blocks, but no syntax highlighting is applied.
a|If this behavior is acceptable, no change is needed.
Otherwise, do one of the following:

* assign the appropriate language to the source block, or
* remove the `source` style and replace it with the `listing` style, or
* remove the `source` style and change the block to a delimited literal block (`+....+`).

|Section and block title substitution order
|The order of substitutions applied to section and block titles now matches the normal substitution order.
This can affect section and block titles that use attribute references.
|Review section and block titles that contain attribute references for errors.

|`a` and `l` column modifiers
|Normal substitutions and default header formatting are now correctly applied to the cells in an implicit header row when the AsciiDoc (`a`) and literal (`l`) modifiers are applied to the columns in a table.
|Update tables that use the `a` and `l` modifiers in combination with an implicit header row so your desired output is displayed.

|`table-topbot` CSS class
|The CSS class `table-ends` replaces the deprecated `table-topbot` CSS class.
|If you customized the styles for `table-topbot` in your UI, update the class name to `table-ends` and build a new UI bundle version.

|"`Unresolved include directive`" message in the content
|The message has changed to "`Unresolved directive`".
|

|`attribute-missing`
|The `attribute-missing` setting is now honored when include directives and block macros are processed.
This may reveal new missing include files and references.
|Check the log messages for new warnings and fix any reported errors.

|Footnotes
|The `footnoteref` macro is deprecated and the structure of the `footnote` macro has changed to be consistent with other AsciiDoc macros.
Previously, the footnote target was placed inside the macro's square brackets.
Now the target is placed directly after the colon (`+footnote:<target>[<optional attributes>]+`).
|Change `footnoteref` to `footnote` and move the footnote target to the correct position.

|Anchors and xrefs in footnotes
|Anchor and xref macros are processed before footnote macros so that footnote macros aren't terminated prematurely.
|Remove escape syntax, such as a backslash (`+\+`), from anchor and xref macros used inside footnote macro attribute lists.

|Encode characters in email address to comply with RFC-3986
|Previously, spaces in an email address were encoded as `%20`.
Now, spaces are encoded as a plus sign (`{plus}`) in email addresses to comply with https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent#Description[RFC-3986].
|

|Description list delimiters (`::`)
|Description list delimiters, that is, double colons (`::`) that are bare or at the start of line are no longer mistaken for a description list item.
|Remove escape syntax around double colons (`::`) that were previously mistaken for description list delimiters.

|Table column width
|The rounding used when calculating table column widths changed minutely.
|No action is needed as the change shouldn't be noticeable to site visitors.

|===

== Semantic versioning and Asciidoctor 2

Starting with version 2.0.0, Asciidoctor and Asciidoctor.js switched to semantic versioning.
This allows Antora to automatically pick up newer patch versions of Asciidoctor.js without making a new Antora release.
